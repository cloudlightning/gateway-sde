/*
 * Copyright 2017 Institute e-Austria Timisoara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.cloudlightning.ieat.sde.rest.schedulers;

import alien4cloud.rest.model.RestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.cloudlightning.bcri.blueprint.BlueprintResourceTemplate;
import eu.cloudlightning.bcri.blueprint.Implementation;
import eu.cloudlightning.bcri.blueprint.ServiceElement;
import eu.cloudlightning.ieat.sde.model.ImplementationRequest;
import eu.cloudlightning.ieat.sde.model.Optimization;
import eu.cloudlightning.ieat.sde.model.RequestStatus;
import eu.cloudlightning.ieat.sde.model.ServiceRequest;
import eu.cloudlightning.ieat.sde.rest.Application;
import eu.cloudlightning.ieat.sde.rest.components.AuthRestTemplate;
import eu.cloudlightning.ieat.sde.rest.components.A4CRestTemplateFactory;
import eu.cloudlightning.ieat.sde.rest.components.RestTemplateFactory;
import eu.cloudlightning.ieat.sde.rest.repositories.OptimizationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static eu.cloudlightning.ieat.sde.rest.Application.A4CEndpoint;
import static eu.cloudlightning.ieat.sde.rest.Application.callBackAddress;
import static eu.cloudlightning.ieat.sde.rest.Application.cellManagerEndpoint;
import static eu.cloudlightning.ieat.sde.rest.components.A4CRestTemplateFactory.createCookieHeaders;

/**
 * Created by adrian on 10.03.2017.
 */
@Controller
public class BlueprintScheduler {

    private static final Logger log = LoggerFactory.getLogger(BlueprintScheduler.class);
    @Autowired
    OptimizationRepository optimizationRepository;
    private static final String resultEndpoint = callBackAddress + "/sde/optimize/{id}";


    @Autowired
    A4CRestTemplateFactory a4CrestTemplateFactory;
    @Autowired
    RestTemplateFactory restTemplateFactory;


    public void submitRequestToCellManager(Optimization o) {

        BlueprintResourceTemplate blueprint = new BlueprintResourceTemplate();
        blueprint.setBlueprintId(o.getId());
        blueprint.setCallbackEndpoint(resultEndpoint.replace("{id}", o.getId()));
        blueprint.setTimestamp(System.currentTimeMillis());
        blueprint.setCost(0.0);
        List<ServiceElement> serviceElements = new ArrayList<ServiceElement>();


        for(ServiceRequest s : o.getRequestList()){
            ServiceElement serviceElement = new ServiceElement();
            serviceElement.setServiceElementId(s.getName());
            List<Implementation> implementations = new ArrayList<Implementation>();
            for(ImplementationRequest impl : s.getImplementationList()) {
                URI compURI = null;
                try {
                    compURI = new URI(A4CEndpoint + "/rest/components/element/" + impl.getElementId() + "/version/" + impl.getArchiveVersion());
                } catch (URISyntaxException e) {
                    log.debug(e.getMessage());
                }
                AuthRestTemplate authRestTemplate = a4CrestTemplateFactory.getObject();
                RestTemplate restTemplate = authRestTemplate.getRestTemplate();
                HttpHeaders cookieHeaders = createCookieHeaders(authRestTemplate.getCookies());
                RequestEntity<String> compRequest = new RequestEntity<String>(null, cookieHeaders, HttpMethod.GET, compURI);
                ResponseEntity<RestResponse> response = restTemplate.exchange(compRequest, RestResponse.class);
                RestResponse nodeTypeRestResponse = response.getBody();
                if(nodeTypeRestResponse.getError() != null){
                    log.error("got error" + nodeTypeRestResponse.getError().toString());
                    return;
                }
                Map nodeMap = (Map) nodeTypeRestResponse.getData();
//                    log.info(toscaNode.getProperties().toString());
                implementations = addImplementation(nodeMap, implementations, impl);
            }
            serviceElement.setImplementations(implementations);
            serviceElements.add(serviceElement);

        }

        blueprint.setServiceElements(serviceElements);
        ObjectMapper mapper = new ObjectMapper();
        try {
            String blueprintString = mapper.writeValueAsString(blueprint);
            String optiString = mapper.writeValueAsString(o);

            log.info("Sending request to Cell Manager");
            log.info("Optimization {}", optiString);
            log.info("Blueprint {}", blueprintString);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        optimizationRepository.save(o);
        RestTemplate restTemplate = restTemplateFactory.getObject();
        HttpEntity<BlueprintResourceTemplate> blueprintRequest = new HttpEntity<BlueprintResourceTemplate>(blueprint,
                RestTemplateFactory.jsonHeaders());
        ResponseEntity<String> response = restTemplate.postForEntity(cellManagerEndpoint + "/resource_template", blueprintRequest, String.class);
        if(response.getStatusCode().equals(HttpStatus.NO_CONTENT) ||
                response.getStatusCode().equals(HttpStatus.BAD_REQUEST) ||
                response.getStatusCode().equals(HttpStatus.NOT_FOUND)){

            //Something went wrong while creating a queue event from the request.
            log.debug("Got [{}]\tCellManager was not able to parse this request: {}",response.getStatusCode(), blueprint);
            o.setStatus(RequestStatus.FAIL);
            optimizationRepository.save(o);
        }else if(response.getStatusCode().equals(HttpStatus.OK)) {
            //Successfully created resource discovery request
            log.debug("Got [{}]\tRequest [{}] is processing",response.getStatusCode(), blueprint.getBlueprintId());
            o.setStatus(RequestStatus.PROCESSING);
            optimizationRepository.save(o);
        }

    }

    private List<Implementation> addImplementation(Map nodeMap, List<Implementation> implementations, ImplementationRequest impl) {


        List<String> virtualizationTypes = new ArrayList<String>();
        String acceleratorReq = null;
        String acceleratorCap = null;
        List requirements = (List) nodeMap.get("requirements");

        for(Object r : requirements){
            Map req  = (Map) r;
            if("host".equals(req.get("id"))){
                String type = (String) req.get("type");
                if(type.contains("Container.CLResource")){
                    virtualizationTypes.add("VM");
                    virtualizationTypes.add("BM");
                }else if(type.contains("Container.Docker")){
                    virtualizationTypes.add("CONTAINER");

                }
            }else if("accelerator".equals(req.get("id"))){
                String type = (String) req.get("type");
                if(type.equals("cloudlightning.capabilities.AcceleratedByMIC")){
                    acceleratorReq = "MIC";
                }else if(type.contains("cloudlightning.capabilities.AcceleratedByGPU")){
                    acceleratorReq = "GPU";
                }else if(type.contains("cloudlightning.capabilities.AcceleratedByDFE")){
                    acceleratorReq = "DFE";
                }

            }
        }

        List capabilities = (List) nodeMap.get("capabilities");
        for(Object c : capabilities){
            Map cap  = (Map) c;
            String type = (String) cap.get("type");
            if(type.equals("cloudlightning.capabilities.MICAttachment")){
                acceleratorCap = "MIC";
            }
            if(type.equals("cloudlightning.capabilities.GPUAttachment")){
                acceleratorCap = "GPU";
            }
            if(type.equals("cloudlightning.capabilities.DFEAttachment")){
                acceleratorCap = "DFE";
            }

        }

        for(String virt : virtualizationTypes){
            Implementation i = basicImplementation();

            if(acceleratorReq == null && acceleratorCap == null){
                acceleratorReq = "CPU";
            }else if(acceleratorReq == null){
                acceleratorReq = acceleratorCap;
            }

            if(acceleratorReq.equals("CPU")){
                if(Application.TELEMETRY) {
                    i.getAcceleratorRange().add(0);
                    i.getAcceleratorRange().add(0);
                }
            }else{

                    i.getAcceleratorRange().add(1);
                    i.getAcceleratorRange().add(1);

            }
            i.setImplementationType(acceleratorReq +  "_" + virt);

            implementations.add(i);
            impl.getSOSMTypes().add(i.getImplementationType());
        }
        return implementations;
    }

    private Implementation basicImplementation() {
        Implementation i = new Implementation();
        i.setAcceleratorRange(new ArrayList<Integer>());
        i.setBandwidthRange(new ArrayList<Double>());
        i.setComputationRange(new ArrayList<Integer>());
        i.setMemoryRange(new ArrayList<Double>());
        i.setStorageRange(new ArrayList<Double>());
        i.setRequiredResourceUnit(1);



        if(Application.TELEMETRY) {
            i.getComputationRange().add(1);
            i.getComputationRange().add(1);
            i.getMemoryRange().add(100.0);
            i.getMemoryRange().add(1000.0);
            i.getBandwidthRange().add(100.0);
            i.getBandwidthRange().add(1000.0);

            i.getStorageRange().add(5.0);
            i.getStorageRange().add(50.0);
        }
        return i;
    }

}
