/*
 * Copyright 2017 Institute e-Austria Timisoara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.cloudlightning.ieat.sde.rest.util;

import eu.cloudlightning.ieat.sde.model.AdditionalOperation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adrian on 13.09.2017.
 */
public class NodeBuilder {
    private static final String version = ":1.0.0-SNAPSHOT";
    public static List<AdditionalOperation> basicContainer(String serviceName, int number){

        List<AdditionalOperation> result = new ArrayList<AdditionalOperation>();
        AdditionalOperation addNodeOp = new AdditionalOperation();
        addNodeOp.put("indexedNodeTypeId", "cloudlightning.nodes.DockerHost" + version);
        addNodeOp.put("nodeName", "DockerHost_"+number);
        addNodeOp.put("type", "org.alien4cloud.tosca.editor.operations.nodetemplate.AddNodeOperation");
        result.add(addNodeOp);


        AdditionalOperation rel = new AdditionalOperation();
        rel.put("nodeName", serviceName);
        rel.put("relationshipName", "hostedOn" + addNodeOp.get("nodeName") + "Host");
        rel.put("relationshipType", "tosca.relationships.HostedOn");
        rel.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel.put("requirementName", "host");
        rel.put("requirementType", "tosca.capabilities.Container.Docker");
        rel.put("target", addNodeOp.get("nodeName"));
        rel.put("targetedCapabilityName", "host");
        rel.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");

        result.add(rel);
        return result;
    }

    public static List<AdditionalOperation> basicCLResource(String serviceName, int number){

        List<AdditionalOperation> result = new ArrayList<AdditionalOperation>();
        AdditionalOperation addNodeOp = new AdditionalOperation();
        addNodeOp.put("indexedNodeTypeId", "cloudlightning.nodes.CLResource" + version);
        addNodeOp.put("nodeName", "CLResource_"+number);
        addNodeOp.put("type", "org.alien4cloud.tosca.editor.operations.nodetemplate.AddNodeOperation");
        result.add(addNodeOp);


        AdditionalOperation rel = new AdditionalOperation();
        rel.put("nodeName", serviceName);
        rel.put("relationshipName", "hostedOn" + addNodeOp.get("nodeName") + "Host");
        rel.put("relationshipType", "tosca.relationships.HostedOn");
        rel.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel.put("requirementName", "host");
        rel.put("requirementType", "tosca.capabilities.Container.CLResource");
        rel.put("target", addNodeOp.get("nodeName"));
        rel.put("targetedCapabilityName", "host");
        rel.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");

        result.add(rel);

        return result;
    }

    public static List<AdditionalOperation> basicMIC(String hostName, String serviceName, int number, String mountingPoint){
        List<AdditionalOperation> result = new ArrayList<AdditionalOperation>();
        AdditionalOperation addNodeOp = new AdditionalOperation();
        addNodeOp.put("indexedNodeTypeId", "cloudlightning.nodes.MIC" + version);
        addNodeOp.put("nodeName", "MIC_"+number);
        addNodeOp.put("type", "org.alien4cloud.tosca.editor.operations.nodetemplate.AddNodeOperation");
        result.add(addNodeOp);


        AdditionalOperation rel = new AdditionalOperation();
        rel.put("nodeName", addNodeOp.get("nodeName"));
        rel.put("relationshipName", "mountAccelerator" + hostName + "Host");
        rel.put("relationshipType", "cloudlightning.relationships.MountAccelerator");
        rel.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel.put("requirementName", "attachment");
        rel.put("requirementType", "cloudlightning.capabilities.AcceleratorAttachment");
        rel.put("target", hostName);
        rel.put("targetedCapabilityName", "accelerator");
        rel.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");
        result.add(rel);

        AdditionalOperation rel2 = new AdditionalOperation();
        rel2.put("nodeName", serviceName);
        rel2.put("relationshipName", "acceleratedBy" + addNodeOp.get("nodeName") + "Accelerator");
        rel2.put("relationshipType", "cloudlightning.relationships.AcceleratedByMIC");
        rel2.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel2.put("requirementName", "accelerator");
        rel2.put("requirementType", "cloudlightning.capabilities.AcceleratedByMIC");
        rel2.put("target", addNodeOp.get("nodeName"));
        rel2.put("targetedCapabilityName", "accelerator");
        rel2.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");
        result.add(rel2);


        addAccelerator(serviceName, mountingPoint, result, rel2);

        return result;

    }

    private static void addAccelerator(String serviceName, String mountingPoint, List<AdditionalOperation> result, AdditionalOperation rel2) {
        if(!"".equals(mountingPoint)) {
            AdditionalOperation updateRel2 = new AdditionalOperation();
            updateRel2.put("nodeName", serviceName);
            updateRel2.put("propertyName", "host_path");
            updateRel2.put("propertyValue", mountingPoint);

            updateRel2.put("relationshipName", rel2.get("relationshipName"));
            updateRel2.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.UpdateRelationshipPropertyValueOperation");
            result.add(updateRel2);
        }
    }

    public static List<AdditionalOperation> basicGPU(String hostName, int number, String serviceName, String mountingPoint){

        List<AdditionalOperation> result = new ArrayList<AdditionalOperation>();
        AdditionalOperation addNodeOp = new AdditionalOperation();
        addNodeOp.put("indexedNodeTypeId", "cloudlightning.nodes.GPU" + version);
        addNodeOp.put("nodeName", "GPU_"+number);
        addNodeOp.put("type", "org.alien4cloud.tosca.editor.operations.nodetemplate.AddNodeOperation");
        result.add(addNodeOp);


        AdditionalOperation rel = new AdditionalOperation();
        rel.put("nodeName", addNodeOp.get("nodeName"));
        rel.put("relationshipName", "mountAccelerator" + hostName + "Host");
        rel.put("relationshipType", "cloudlightning.relationships.MountAccelerator");
        rel.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel.put("requirementName", "attachment");
        rel.put("requirementType", "cloudlightning.capabilities.AcceleratorAttachment");
        rel.put("target", hostName);
        rel.put("targetedCapabilityName", "accelerator");
        rel.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");
        result.add(rel);

        AdditionalOperation rel2 = new AdditionalOperation();
        rel2.put("nodeName", serviceName);
        rel2.put("relationshipName", "acceleratedBy" + addNodeOp.get("nodeName") + "Accelerator");
        rel2.put("relationshipType", "cloudlightning.relationships.AcceleratedByGPU");
        rel2.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel2.put("requirementName", "accelerator");
        rel2.put("requirementType", "cloudlightning.capabilities.AcceleratedByGPU");
        rel2.put("target", addNodeOp.get("nodeName"));
        rel2.put("targetedCapabilityName", "accelerator");
        rel2.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");
        result.add(rel2);

        addAccelerator(serviceName, mountingPoint, result, rel2);


        return result;

    }

    public static List<AdditionalOperation> basicDFE(String hostName, int number, String serviceName, String mountingPoint){
        List<AdditionalOperation> result = new ArrayList<AdditionalOperation>();
        AdditionalOperation addNodeOp = new AdditionalOperation();
        addNodeOp.put("indexedNodeTypeId", "cloudlightning.nodes.DFE" + version);
        addNodeOp.put("nodeName", "DFE_"+number);
        addNodeOp.put("type", "org.alien4cloud.tosca.editor.operations.nodetemplate.AddNodeOperation");
        result.add(addNodeOp);


        AdditionalOperation rel = new AdditionalOperation();
        rel.put("nodeName", addNodeOp.get("nodeName"));
        rel.put("relationshipName", "mountAccelerator" + hostName + "Host");
        rel.put("relationshipType", "cloudlightning.relationships.MountAccelerator");
        rel.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel.put("requirementName", "attachment");
        rel.put("requirementType", "cloudlightning.capabilities.AcceleratorAttachment");
        rel.put("target", hostName);
        rel.put("targetedCapabilityName", "accelerator");
        rel.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");
        result.add(rel);

        AdditionalOperation rel2 = new AdditionalOperation();
        rel2.put("nodeName", serviceName);
        rel2.put("relationshipName", "acceleratedBy" + addNodeOp.get("nodeName") + "Accelerator");
        rel2.put("relationshipType", "cloudlightning.relationships.AcceleratedByDFE");
        rel2.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel2.put("requirementName", "accelerator");
        rel2.put("requirementType", "cloudlightning.capabilities.AcceleratedByDFE");
        rel2.put("target", addNodeOp.get("nodeName"));
        rel2.put("targetedCapabilityName", "accelerator");
        rel2.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");
        result.add(rel2);

        return result;

    }

    public static List<AdditionalOperation> basicRemoteDFE(String hostName, String serviceName, int number, String mountingPoint){
        List<AdditionalOperation> result = new ArrayList<AdditionalOperation>();
        AdditionalOperation addNodeOp = new AdditionalOperation();
        addNodeOp.put("indexedNodeTypeId", "cloudlightning.nodes.RemoteDFE" + version);
        addNodeOp.put("nodeName", "RemoteDFE_"+number);
        addNodeOp.put("type", "org.alien4cloud.tosca.editor.operations.nodetemplate.AddNodeOperation");
        result.add(addNodeOp);


        AdditionalOperation rel = new AdditionalOperation();
        rel.put("nodeName", addNodeOp.get("nodeName"));
        rel.put("relationshipName", "mountAccelerator" + hostName + "Host");
        rel.put("relationshipType", "cloudlightning.relationships.MountAccelerator");
        rel.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel.put("requirementName", "attachment");
        rel.put("requirementType", "cloudlightning.capabilities.AcceleratorAttachment");
        rel.put("target", hostName);
        rel.put("targetedCapabilityName", "accelerator");
        rel.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");
        result.add(rel);

        AdditionalOperation rel2 = new AdditionalOperation();
        rel2.put("nodeName", serviceName);
        rel2.put("relationshipName", "acceleratedBy" + addNodeOp.get("nodeName") + "Accelerator");
        rel2.put("relationshipType", "cloudlightning.relationships.AcceleratedByDFE");
        rel2.put("relationshipVersion","1.0.0-SNAPSHOT");
        rel2.put("requirementName", "accelerator");
        rel2.put("requirementType", "cloudlightning.capabilities.AcceleratedByDFE");
        rel2.put("target", addNodeOp.get("nodeName"));
        rel2.put("targetedCapabilityName", "accelerator");
        rel2.put("type", "org.alien4cloud.tosca.editor.operations.relationshiptemplate.AddRelationshipOperation");
        result.add(rel2);

        return result;
    }
}
