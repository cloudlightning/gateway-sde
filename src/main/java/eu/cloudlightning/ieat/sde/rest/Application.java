/*
 * Copyright 2017 Institute e-Austria Timisoara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.cloudlightning.ieat.sde.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by adrian on 08.03.2017.
 */
@SpringBootApplication
public class Application {

    private static final String propertiesFile = "/conf/sde.conf";

    public static String callBackAddress;
    public static String cellManagerEndpoint;
    public static final String A4CEndpoint = "http://localhost:8091";
    public static final String brooklynEndpoint = "http://localhost:8081";
    public static boolean TELEMETRY;
    public static final String brooklynUser = "admin";
    public static final String brooklynPassword = "admin";

    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();

        properties.load(Application.class.getResourceAsStream(propertiesFile));
        callBackAddress = properties.getProperty("self.assignedIp");
        cellManagerEndpoint = properties.getProperty("cellManager.endpoint");
        TELEMETRY = Boolean.parseBoolean(properties.getProperty("cellManager.telemetry"));
        SpringApplication.run(Application.class, args);
    }
}
